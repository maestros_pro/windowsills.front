import * as THREE from "three";
let OrbitControls = require('three-orbit-controls')(THREE);
import {OBJLoader, MTLLoader} from 'three-obj-mtl-loader';

export default class Model3D {
	constructor(options) {

		this.canvas = null;

		Object.assign(this._options = {}, this._default(), options);
		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this.init();
			});
		} else {
			this.init();
		}
	}


	_default() {
		return {
			element: false
		}
	}

	_draw(){


		this.renderer = new THREE.WebGLRenderer( { alpha: true, antialias: true } );
		this.renderer.setSize( this.el.clientWidth, this.el.clientHeight );
		this.el.appendChild( this.renderer.domElement );

		this.scene = new THREE.Scene();
		this.scene.add( new THREE.AmbientLight( 0xffffff, 0.2 ) );

		this.camera = new THREE.PerspectiveCamera( 20, this.el.clientWidth / this.el.clientHeight, 1, 500 );
		this.camera.up.set( 0, 0, 1 );
		this.camera.position.set( 0, 50, 0 );
		this.scene.add( this.camera );

		this.controls = new OrbitControls( this.camera, this.renderer.domElement );
		this.controls.addEventListener( 'change', this._render.bind(this) );
		this.controls.minDistance = 300;
		this.controls.maxDistance = 300;
		this.controls.enableZoom = false;
		this.controls.enablePan = false;
		this.controls.target.set( 0, 0, 10 );
		this.controls.update();

		this.camera.add( new THREE.PointLight( 0xffffff, 0.8 ) );

		let loader = new OBJLoader();

		loader.load(this._options.object, ( object )=>{
			this.object = object;

			this.object.scale.set(this._options.objectScale, this._options.objectScale, this._options.objectScale);
			this.object.position.x = 0;
			this.object.position.y = 0;
			this.object.position.z = 30;

			this.object.rotation.x = -90 * Math.PI / 180;
			this.object.rotation.z = 180 * Math.PI / 180;
			this.object.rotation.y = 45 * Math.PI / 180;

			this.scene.add( this.object );
			this._render.bind(this);

			this.changeColor(this._options.objectColor);
		} );

	}

	_render(){
		this.renderer.render( this.scene, this.camera )
	}

	_animate(){
		window.requestAnimationFrame( this._animate.bind(this) );

		// this.mesh.rotation.x += 0.01;
		// this.mesh.rotation.y += 0.02;

		this._render();
	}

	_resize(){
		this.camera.aspect = this.el.clientWidth / this.el.clientHeight;
		this.camera.updateProjectionMatrix();

		this.renderer.setSize( this.el.clientWidth, this.el.clientHeight );
	}

	init(){

		this.el = document.querySelectorAll(this._options.element)[0];

		this._draw();
		this._animate();

		window.addEventListener( 'resize', this._resize.bind(this), false );
	}


}
