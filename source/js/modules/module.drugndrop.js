
export default class DrugNDrop{
	constructor(options) {

		this._polyfill();

		Object.assign(this._options = {}, this._default(), options);
		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this._init();
			});
		} else {
			this._init();
		}
	}

	_polyfill() {

		//assign
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {
					'use strict';
					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}

		//closest
		if (!Element.prototype.closest) {
			Element.prototype.closest = function (css) {
				let node = this;
				while (node) {
					if (node.matches(css)) return node;
					else node = node.parentElement;
				}
				return null;
			};
		}
	}

	_default() {
		return {
			element: '.drugndrop__box',
			output: '.drugndrop__list',
			fileInputId: 'drugndropFileInput',
			fileInputName: 'file',
			maxFiles: 10,
			maxFileSize: 15000000,

			classHover: 'is-drugndrop',
			classBody: 'is-drug-over',
			autoupload: false,
			url: '',

			itemTemplate: (item)=>{

				/**
				 * possible values:
				 * {name} - file name with extension
				 * {shortName} - file name without extension
				 * {extension} - file extension
				 * {size} - file size in bites
				 * {type} - type of file
				 * {id} - local id of loaded file
				 * {src} - for type image files
				 * {progress} - progress line autoupload image
				 */

				let
					image = item.src ? '<div class="drugndrop__item-image"><img src="{src}"></div>' : '',
					size = item.size.toString().length > 6 ? item.size.toString().slice(0,-6) + 'Мбайт' : item.size.toString().length > 3 ? item.size.toString().slice(0,-3) + 'Кбайт' : item.size,
					progress = item.progress ? `{progress}
								<div class="drugndrop__progress">
									<div class="drugndrop__progress-value"></div>
									<div class="drugndrop__progress-bar"></div>
								</div>` : ''
					// extension = item.name.match(/\w+/ig).pop(),
					// name = item.name.replace(`.${extension}`, '')
				;

				/**
				 * data-remove - is require data attribute for remove button
				 */
				return `<div class="drugndrop__item">
							<div class="drugndrop__item-inner">
								<div class="drugndrop__item-remove" data-remove="{id}"></div>
								<div class="drugndrop__item-name">{name}</div>
								<div class="drugndrop__item-size">${size}</div>
								<div class="drugndrop__item-type">{extension} ({type})</div>
								${progress}
								${image}
							</div>
						</div>`
			},

			/**
			 * callback functions:
			 */
			// fileUploadProgress(progress, item){},
			// onFileUploaded(id, item){},
			// onFileAdded(obj){},
			// onFileRemoved(id){},
			// onOverSize(name){},
			// onOverLimit(){}

		}
	}


	_templateParser(template, data) {
		return template ? template.replace(/\{([^\}]+)\}/g, function (value, key) {
			return key in data ? data[key] : '';
		}).replace(/&([^\=]+)\=\{([^\}]+)\}/g, '') : '';
	}

	_filesAdd(files){
		// console.info('_fileParser', files);

		([...files]).forEach((file)=>{
			this._fileParser(file);
		})

	}

	_fileParser(file){
		// console.info('_fileItem', file);

		let obj = {};

		obj.file = file;
		obj.data = {};
		obj.data.name = file.name;
		obj.data.extension = file.name.match(/\w+/ig).pop();
		obj.data.shortName = file.name.replace(`.${obj.data.extension}`, '');
		obj.data.size = file.size;
		obj.data.type = file.type;

		if ( /^(image)/ig.test(file.type) ){
			let reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onloadend = ()=>{
				obj.data.src = reader.result;
				this._addItem(obj);
			};
		} else {
			this._addItem(obj);
		}
	}

	_setUploadProgress(id, progress){
		let item = this.dropArea.querySelector(`[data-id="${id}"]`);
		item.getElementsByTagName('progress')[0].value = parseInt(progress);
		if ( this._options.fileUploadProgress && typeof this._options.fileUploadProgress === 'function') this._options.fileUploadProgress(progress, item);

		if ( progress === 100 ){
			if ( this._options.onFileUploaded && typeof this._options.onFileUploaded === 'function') this._options.onFileUploaded(id, item);
		}

	}

	_addItem(obj){
		obj.data.id = '_' + Math.random().toString(36).substr(2, 9);

		if (this._options.maxFiles && this.files.length >= this._options.maxFiles) {
			console.warn(`Превышено максимальное количество файлов (${this._options.maxFiles})`);
			if (this._options.onOverLimit && typeof this._options.onOverLimit === 'function') this._options.onOverLimit();
			return false;
		}

		if (this._options.maxFileSize && obj.file.size > this._options.maxFileSize) {
			console.warn(`Превышен максимальный вес файла (${obj.file.size} из ${this._options.maxFileSize})`);
			if (this._options.onOverSize && typeof this._options.onOverSize === 'function') this._options.onOverSize(obj.data.name);
			return false;
		}

		if ( this._checkAddedFile(obj.file) ) return false;

		this.files.push(obj);

		let wrapper = document.createElement('div'), item;

		obj.data.progress = `<progress max="100" value="0"></progress>`;

		wrapper.innerHTML = this._templateParser(this._options.itemTemplate(obj.data), obj.data).trim();
		item = wrapper.firstChild;
		item.setAttribute('data-id', obj.data.id);

		this.dropOutput.appendChild(item);

		if (this._options.onFileAdded && typeof this._options.onFileAdded === 'function') this._options.onFileAdded(obj);

		if (this._options.autoupload && this._options.url){
			this._uploadFile(obj);
		}
	}

	_onDrop(e){
		let collect = e.dataTransfer || e.target;
		this._filesAdd(collect.files)
	}

	_dropBodyActions(e){
		let body = document.body;

		['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
			body.addEventListener(eventName, (e)=>{
				e.preventDefault();
			}, false)
		});
		['dragenter', 'dragover'].forEach(eventName => {
			body.addEventListener(eventName, ()=>{
				body.classList.add(this._options.classBody)
			}, false)
		});
		['dragleave', 'drop'].forEach(eventName => {
			body.addEventListener(eventName, ()=>{
				body.classList.remove(this._options.classBody)
			}, false)
		});
	}

	_dropAreaActions(){
		['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
			this.dropArea.addEventListener(eventName, (e)=>{
				e.preventDefault();
			}, false)
		});
		['dragenter', 'dragover'].forEach(eventName => {
			this.dropArea.addEventListener(eventName, ()=>{
				this.dropArea.classList.add(this._options.classHover)
			}, false)
		});
		['dragleave', 'drop'].forEach(eventName => {
			this.dropArea.addEventListener(eventName, ()=>{
				this.dropArea.classList.remove(this._options.classHover)
			}, false)
		});

		this.dropOutput.addEventListener('click', (e)=>{
			if ( e.target.closest('[data-remove]') ) {
				e.preventDefault();
				this.remove(e.target.closest('[data-remove]').getAttribute('data-remove'));
			}

		}, false);
	}

	_createFileInput(){
		this.fileInput = document.createElement('input');
		this.fileInput.setAttribute('type', 'file');
		this.fileInput.setAttribute('id', this._options.fileInputId);
		this.fileInput.style.position = 'absolute';
		this.fileInput.style.opacity = '0';
		this.fileInput.style.pointerEvents = 'none';
		if (this._options.maxFiles && this._options.maxFiles > 1) this.fileInput.setAttribute('multiple', 'multiple');
		this.dropArea.appendChild(this.fileInput);

		this.fileInput.addEventListener('change', this._onDrop.bind(this));
	}

	_checkAddedFile(file){

		let filIsAdded = false;

		for(let i = 0; i < this.files.length; i++){
			if (file.size === this.files[i].file.size && file.name === this.files[i].file.name && file.type === this.files[i].file.type ) {
				filIsAdded = true;
				break;
			}
		}

		return filIsAdded;
	}

	_init() {

		this.files = [];
		this.dropArea = typeof this._options.element === 'string' ? document.querySelectorAll(this._options.element)[0] : this._options.element;
		this.dropOutput = typeof this._options.element === 'string' ? document.querySelectorAll(this._options.output)[0] : this._options.output;

		if ( this.dropArea.length && this.dropOutput.length){
			this._createFileInput();

			this._dropBodyActions();
			this._dropAreaActions();

			this.dropArea.addEventListener('drop', this._onDrop.bind(this), false);
		}

	};

	_uploadFile(obj) {

		console.info(obj);

		let xhr = new XMLHttpRequest(),
			formData = new FormData();

		xhr.open('POST', this._options.url, true);

		// xhr.setRequestHeader('Content-Type', 'multipart/form-data');
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.upload.addEventListener("progress", (e)=>{
			let progress = (e.loaded * 100.0 / e.total) || 100;

			this._setUploadProgress(obj.data.id, progress);

		});

		xhr.addEventListener('readystatechange', (e)=>{
			if (xhr.readyState === 4 && xhr.status === 200) {
				// Готово. Сообщаем пользователю
			}
			else if (xhr.readyState === 4 && xhr.status !== 200) {
				// Ошибка. Сообщаем пользователю
				console.error('ошибка загрузки файла', e);
			}
		});

		formData.append('id', obj.data.id);
		formData.append(this._options.fileInputName, obj.file);
		xhr.send(formData);
	}

	remove(id){
		let i = 0;
		for (i; i < this.files.length; i++ ){
			if (this.files[i].data.id === id ) break;
		}
		this.files.splice(i, 1);
		this.dropOutput.querySelectorAll(`[data-id=${id}]`)[0].remove();

		if (this._options.onFileRemoved && typeof this._options.onFileRemoved === 'function') this._options.onFileRemoved(id);
	}

}