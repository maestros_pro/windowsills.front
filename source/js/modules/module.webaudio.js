'use strict';

export default class Collection {
	constructor(options) {

		Object.assign(this.options = {}, this._default(), options);

		this.status = 0;
		this.current = 0;
		this.storage = [];
		// this.context = context;
		this.context = new window.AudioContext();
		this.sample = null;
		this.timeout = null;
		this.analizerStop = false;
		this.isPlaying = false;
	}

	_default() {
		return {
			autoplay: false,
			delay: 0,
			analize: false,
			onSampleAnalize: function () {
			},
			onSamplePlay: function () {
			},
			onSampleEnd: function () {
			},
			onSamplePause: function () {
			}
		}
	}

	_initAnalizer(){

		this.analyser = this.context.createAnalyser();
		this.freqs = new Uint8Array(this.analyser.frequencyBinCount);
		this.times = new Uint8Array(this.analyser.frequencyBinCount);

		this.analyser.connect(this.context.destination);
		this.analyser.minDecibels = -140;
		this.analyser.maxDecibels = 0;

		if ( !this.analizerStop ){
			this._drawAnalizer();
		}

	}

	_drawAnalizer(){

		this.analyser.smoothingTimeConstant = 0.8;
		this.analyser.fftSize = 2048;

		// Get the frequency data from the currently playing music
		this.analyser.getByteFrequencyData(this.freqs);
		this.analyser.getByteTimeDomainData(this.times);

		let data = [];

		data.count = this.analyser.frequencyBinCount;
		data.freqs = this.freqs;
		data.times = this.times;

		if ( typeof this.options.onSampleAnalize === 'function') this.options.onSampleAnalize(data);

		if (this.isPlaying) {
			requestAnimationFrame(this._drawAnalizer.bind(this));
		}
	}

	addSounds(arr){
		let i = 0, c = 0;
		for (i; i < arr.length; i++){
			this._loadSoundURL(arr[i], ()=>{
				c++;
				if (this.options.autoplay && c === arr.length) this.play();
			});
		}
	}

	loadSoundBuffer(string){
		let index = this.storage.length;
		this.storage[index] = {};
		this.context.decodeAudioData(
			this._stringToBuffer(string),
			buffer => {
				console.info(buffer);
				this.storage[index].buffer = buffer;
				this.storage[index].duration = buffer.duration;
			},
			e => {
				console.error( "Error with decoding audio data" + e.err );
			});
	}

	loadSound(url) {
		let request = new XMLHttpRequest(),
			index = this.storage.length;
		this.storage[index] = {};
		request.open('GET', url, true);
		request.onload = ()=>{
			console.info(typeof request.response);
			this.context.decodeAudioData(
				this._stringToBuffer(request.response),
				buffer => {
					this.storage[index].buffer = buffer;
					this.storage[index].duration = buffer.duration;
				},
				e => {
					console.error( "Error with decoding audio data" + e.err );
				});
		};
		request.send();
	}

	_loadSoundURL(url, callback) {
		let request = new XMLHttpRequest(),
			index = this.storage.length;
		this.storage[index] = {};
		request.open('GET', url, true);
		request.responseType = 'arraybuffer';
		request.onload = ()=>{
			this.context.decodeAudioData(
				request.response,
				buffer => {
					this.storage[index].buffer = buffer;
					this.storage[index].duration = buffer.duration;
					if ( callback && typeof callback === 'function') callback();
				},
				e => {
					console.error( "Error with decoding audio data" + e.err );
				});

		};
		request.send();
	}

	_stringToBuffer(string){

		let binary = window.atob(string),
			buffer = new ArrayBuffer(binary.length),
			bytes = new Uint8Array(buffer);
		for (let i = 0; i < buffer.byteLength; i++) {
			bytes[i] = binary.charCodeAt(i) & 0xFF;
		}
		return buffer;
	}

	_playSample(){

		if ( this.current >= this.storage.length ) {
			if ( typeof this.options.onSampleEnd === 'function') this.options.onSampleEnd();
			this.stop();
			return false;
		}

		if ( typeof this.options.onSamplePlay === 'function') this.options.onSamplePlay(this.storage[this.current], this.current, this.storage.length);

		this.sample = this.context.createBufferSource();
		this.sample.buffer = this.storage[this.current].buffer;
		this.sample.connect(this.context.destination);

		if ( this.options.analize ) {
			this._initAnalizer();
			this.sample.connect(this.analyser);
		}
		this.sample.start();
		this.timeout = setTimeout(()=>{
			this.current++;
			this._playSample();
		}, (this.storage[this.current].duration * 1000) + this.options.delay);
	}

	play(){
		if ( this.status !== 1 ){
			this.status = 1;
			this.isPlaying = true;
			this._playSample();
		}
	}

	pause(){
		this.status = 0;
		this.sample.stop();
		clearTimeout(this.timeout);
		this.current = this.current > 0 ? this.current - 1 : this.current;
	}

	stop(){
		this.status = 0;
		this.sample.stop();
		this.isPlaying = false;
		clearTimeout(this.timeout);
		this.current = 0;
	}

}
